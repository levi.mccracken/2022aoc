mod days {
    automod::dir!(pub "src/days");
}
pub use days::*;

const SOLVERS: &[fn(release: bool)] = &[
    // day1::solve,
    // day2::solve,
    // day3::solve,
    // day4::solve,
    // day5::solve,
    // day6::solve,
    // day7::solve,
    // day8::solve,
    // day9::solve,
    // day10::solve,
    // day11::solve,
    // day12::solve,
    day13::solve,
];

pub fn main() {
    for (day, solver) in SOLVERS.iter().enumerate() {
        let day = day + 1;
        println!("\n#########################");
        println!("day {day} Test: ");
        solver(false);
        println!("\nday {day} My Data: ");
        solver(true);
    }
}