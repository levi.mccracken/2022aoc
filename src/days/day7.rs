use std::cmp;

struct File{
    size: u32,
    name: String,
}
impl File{
    pub fn print(&self, spaces: usize){
        println!("{0:>spaces$}\t{1}", self.name, self.size);
    }
}

struct Dir{
    dirs: Box<Vec<Dir>>,
    files: Vec<File>,
    name: String,
    size: Option<u32>,
}
impl Dir{
    pub fn insert_dir(&mut self, cwd: &Vec<String>, name: &str, index: usize){
        if index == cwd.len(){
            self.dirs.push(Dir{dirs: Box::new(Vec::new()), files: Vec::new(), name: name.to_string(), size: None});
        }else{
            for count in 0..self.dirs.len(){
                if self.dirs[count].name == cwd[index]{
                    self.dirs[count].insert_dir(cwd, name, index+1);
                    break;
                }
            }
        }
    }  
    pub fn insert_file(&mut self, cwd: &Vec<String>, size: &str, name: &str, index: usize){
        if index == cwd.len(){
            self.files.push(File{name: name.to_string(), size: size.parse().unwrap()});
        }else{
            for count in 0..self.dirs.len(){
                if self.dirs[count].name == cwd[index]{
                    self.dirs[count].insert_file(cwd, size, name, index+1);
                    break;
                }
            }
        }
    }
    pub fn print(&self, spaces: usize){
        if self.size == None{
            println!("{0:>spaces$}", self.name);
        }else{
            println!("{0:>spaces$}-{1}", self.name, self.size.unwrap());
        }
        for count in 0..self.files.len(){
            self.files[count].print(spaces+2);
        }
        for count in 0..self.dirs.len(){
            self.dirs[count].print(spaces+2);
        }
    }
    pub fn update_size(&mut self) -> u32{
        let mut sum = 0;
        for count in 0..self.files.len(){
            sum += self.files[count].size;
        }
        for count in 0..self.dirs.len(){
            sum += self.dirs[count].update_size();
        }
        self.size = Some(sum);
        sum
    }
    pub fn add_under_limit(&mut self, limit: u32) -> u32{
        let mut sum = 0;
        if limit >= self.size.unwrap(){
            sum += self.size.unwrap();
        }
        for count in 0..self.dirs.len(){
            sum += self.dirs[count].add_under_limit(limit);
        }
        sum
    }
    pub fn find_smallest_over_limit(&mut self, limit: u32) -> Option<u32>{
        let mut smallest = None;
        if limit <= self.size.unwrap(){
            smallest = self.size;
        }
        for count in 0..self.dirs.len(){
            let dir_size = self.dirs[count].find_smallest_over_limit(limit);
            match dir_size{
                Some(i) => {
                    match smallest{
                        Some(j) => smallest = Some(cmp::min(i, j)),
                        None => smallest = Some(i),
                    }
                },
                None => {},
            }
        }
        smallest
    }
}

pub fn solve_helper(input: &str) {
    let mut cwd: Vec<String> = Vec::new();
    let mut head = Dir{dirs: Box::new(Vec::new()), files: Vec::new(), name: "/".to_string(), size: None};

    let mut input_commands = input.split("$ ");
    input_commands.next();
    loop{
        let command_option = input_commands.next();
        if command_option == None{
            break;
        }
        let command = command_option.unwrap();
        if "cd" == &command[0..2]{
            let path = &command[3..command.len()-1];
            if path == ".."{
                cwd.pop();
            }else if path == "/"{
                cwd = Vec::new();
            }else{
                cwd.push(path.to_string());
            }
        }else if "ls" == &command[0..2]{
            let mut entries = command.split("\n");
            entries.next();
            loop{
                let entry_option = entries.next();
                if None == entry_option{
                    break;
                }
                let entry = entry_option.unwrap();
                if "" == entry{
                    break;
                }
                if "dir" == &entry[0..3]{
                    head.insert_dir(&cwd, &entry[4..], 0);
                }else{
                    head.insert_file(&cwd, &entry[0..entry.find(" ").unwrap()], &entry[entry.find(" ").unwrap()..], 0);
                }
            }
        }
    }
    //head.print(0);
    head.update_size();
    //println!("\n\n");
    //head.print(0);
    
    println!("\n\n");
    let part1 = head.add_under_limit(100000);
    println!("part1: {}", part1);
    
    let need_to_be_free = 30000000;
    let total = 70000000;
    let current = head.size.unwrap();
    let current_free = total - current;
    let need_to_free = need_to_be_free - current_free;
    //println!("\n\n");
    println!("part2:Need to delete: {}  Found: {}", need_to_free, head.find_smallest_over_limit(need_to_free).unwrap());
}

pub fn solve(release: bool){
    let input: &str;
    if release{
        input = include_str!("../inputs/day_07_release.txt");
    }else{
        input = include_str!("../inputs/day_07_test.txt");
    }
    solve_helper(input);
}
