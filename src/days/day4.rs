fn solve_01(input: &str) {
    let mut count = 0;
    for line in input.lines(){
        let mut line_split = line.split(",");
        let first_elf = line_split.next().unwrap();
        let second_elf = line_split.next().unwrap();
        let mut fes = first_elf.split("-");
        let mut ses = second_elf.split("-");
        let fe1 = match fes.next().unwrap().parse::<i32>(){
            Ok(fe1) => fe1,
            Err(_e) => -1,
        };
        let fe2 = match fes.next().unwrap().parse::<i32>(){
            Ok(fe2) => fe2,
            Err(_e) => -1,
        };
        let se1 = match ses.next().unwrap().parse::<i32>(){
            Ok(se1) => se1,
            Err(_e) => -1,
        };
        let se2 = match ses.next().unwrap().parse::<i32>(){
            Ok(se2) => se2,
            Err(_e) => -1,
        };
        //println!("{}-{},{}-{}", fe1, fe2, se1, se2);
        if fe1 <= se1 && fe2 >= se2{
            //println!("*");
            count += 1;
        }
        else if fe1 >= se1 && fe2 <= se2{
            //println!("*");
            count += 1;
        }
    }
    println!("{}", count);
}

fn solve_02(input: &str) {
    let mut count = 0;
    for line in input.lines(){
        let mut line_split = line.split(",");
        let first_elf = line_split.next().unwrap();
        let second_elf = line_split.next().unwrap();
        let mut fes = first_elf.split("-");
        let mut ses = second_elf.split("-");
        let fe1 = match fes.next().unwrap().parse::<i32>(){
            Ok(fe1) => fe1,
            Err(_e) => -1,
        };
        let fe2 = match fes.next().unwrap().parse::<i32>(){
            Ok(fe2) => fe2,
            Err(_e) => -1,
        };
        let se1 = match ses.next().unwrap().parse::<i32>(){
            Ok(se1) => se1,
            Err(_e) => -1,
        };
        let se2 = match ses.next().unwrap().parse::<i32>(){
            Ok(se2) => se2,
            Err(_e) => -1,
        };
        //println!("{}-{},{}-{}", fe1, fe2, se1, se2);
        if fe1 <= se1 && fe2 >= se2{
            //println!("*");
            count += 1;
        }
        else if fe1 >= se1 && fe2 <= se2{
            //println!("*");
            count += 1;
        }
        else if fe1 <= se1 && fe2 >= se1{
            //println!("*");
            count += 1;
        }
        else if se1 <= fe1 && se2 >= fe1{
            //println!("*");
            count += 1;
        }
    }
    println!("{}", count);
}

pub fn solve(release: bool){
    let input: &str;
    if release{
        input = include_str!("../inputs/day_04_release.txt");
    }else{
        input = include_str!("../inputs/day_04_test.txt");
    }
    solve_01(input);
    solve_02(input);
}