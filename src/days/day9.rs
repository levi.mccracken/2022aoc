
use num::abs;

struct Pos{
    x: usize,
    y: usize,
}
impl Pos{
    fn follow(&mut self, x: usize, y: usize){
        let mut touching = true;
        if abs(x as i32 - self.x as i32) >= 2{
            touching = false;
        }else if abs(y as i32 - self.y as i32) >= 2{
            touching = false;
        }
        
        if !touching{
            let x_diff = x as i32 - self.x as i32;
            let y_diff = y as i32 - self.y as i32;
            if x_diff >= 1{
                self.x += 1;
            }else if x_diff <= -1{
                self.x -= 1;
            }
            if y_diff >= 1{
                self.y += 1;
            }else if y_diff <= -1{
                self.y -= 1;
            }
        }
    }
}

fn solve_helper(input: &str, rope_len: usize) -> i32{
    let mut grid: Vec<Vec<char>> =  Vec::new();
    grid.push(Vec::new());
    grid[0].push('s');
    
    let mut rope: Vec<Pos> = Vec::new();
    for _i in 0..rope_len{
        rope.push(Pos{x: 0, y: 0});
    }
    let tail_knot = rope_len - 1;
    
    for line in input.lines(){
        let line_chars: Vec<char> = line.chars().collect();
        match line_chars[0]{
            'U' => {
                for _count in 0..(line_chars[2..].iter().collect::<String>().parse().unwrap()){
                    rope[0].y += 1;
                    if rope[0].y >= grid.len(){
                        grid.push(Vec::new());
                        for _i in 0..grid[0].len(){
                            grid[rope[0].y].push('.');
                        }
                    }
                    for knot in 1..rope_len{
                        let x = rope[knot-1].x;
                        let y = rope[knot-1].y;
                        rope[knot].follow(x, y);
                    }
                    grid[rope[tail_knot].y][rope[tail_knot].x] = '#';
                }
            },
            'D' => {
                for _count in 0..(line_chars[2..].iter().collect::<String>().parse().unwrap()){
                    if rope[0].y == 0{
                        grid.push(Vec::new());
                        let grid_y_len = grid.len();
                        for _i in 0..grid[0].len(){
                            grid[grid_y_len - 1].push('.');
                        }
                        grid.rotate_right(1);
                        for knot in 1..rope_len{
                            rope[knot].y += 1;
                        }
                    }else{
                        rope[0].y -= 1;
                    }
                    for knot in 1..rope_len{
                        let x = rope[knot-1].x;
                        let y = rope[knot-1].y;
                        rope[knot].follow(x, y);
                    }
                    grid[rope[tail_knot].y][rope[tail_knot].x] = '#';
                }
            },
            'L' => {
                for _count in 0..(line_chars[2..].iter().collect::<String>().parse().unwrap()){
                    if rope[0].x == 0{
                        for row_num in 0..grid.len(){
                            grid[row_num].push('.');
                            grid[row_num].rotate_right(1);
                        }
                        for knot in 1..rope_len{
                            rope[knot].x += 1;
                        }
                    }else{
                        rope[0].x -= 1;
                    }
                    for knot in 1..rope_len{
                        let x = rope[knot-1].x;
                        let y = rope[knot-1].y;
                        rope[knot].follow(x, y);
                    }
                    grid[rope[tail_knot].y][rope[tail_knot].x] = '#';
                }
            },
            'R' => {
                for _count in 0..(line_chars[2..].iter().collect::<String>().parse().unwrap()){
                    rope[0].x += 1;
                    if rope[0].x >= grid[0].len(){
                        for row_num in 0..grid.len(){
                            grid[row_num].push('.');
                        }
                    }
                    for knot in 1..rope_len{
                        let x = rope[knot-1].x;
                        let y = rope[knot-1].y;
                        rope[knot].follow(x, y);
                    }
                    grid[rope[tail_knot].y][rope[tail_knot].x] = '#'; 
                }
            },
            _ => panic!(),
        }
    }
    
    let mut tail_pos_count = 0;
    for row in grid.iter().rev(){
        for c in row{
            //print!("{}", *c);
            if *c == '#'{
                tail_pos_count += 1;
            }
        }
        //println!("");
    }
    tail_pos_count
}

pub fn solve(release: bool){
    let input: &str;
    if release{
        input = include_str!("../inputs/day_09_release.txt");
    }else{
        input = include_str!("../inputs/day_09_test.txt");
    }
    println!("Part 1: {}", solve_helper(input, 2));
    println!("Part 2: {}", solve_helper(input, 10));
}
