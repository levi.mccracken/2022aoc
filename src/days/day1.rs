use std::str::FromStr;

fn solve_01(input: &str) {
    let lines = input.lines();
    let mut elf = 0;
    let mut elves: Vec<i32> = Vec::new();
    elves.push(0);
    for line in lines{
        if line == ""{
            elf += 1;
            elves.push(0);
            continue;
        }
        let num: i32 = FromStr::from_str(line).unwrap();
        elves[elf] += num;
    }
    println!("Part 1: {}", elves.iter().max().unwrap());
}

fn solve_02(input: &str) {
    let lines = input.lines();
    let mut elf = 0;
    let mut elves: Vec<i32> = Vec::new();
    elves.push(0);
    for line in lines{
        if line == ""{
            elf += 1;
            elves.push(0);
            continue;
        }
        let num: i32 = FromStr::from_str(line).unwrap();
        elves[elf] += num;
    }
    elves.sort();
    elves.reverse();
    println!("Part 2: {}", elves[0] + elves[1] + elves[2]);
}

pub fn solve(release: bool){
    let input: &str;
    if release{
        input = include_str!("../inputs/day_01_release.txt");
    }else{
        input = include_str!("../inputs/day_01_test.txt");
    }
    solve_01(input);
    solve_02(input);
}
