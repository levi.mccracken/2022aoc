
use std::collections::BTreeMap;

fn recurse(dimx: usize, dimy: usize, inspect: (usize, usize), mut depth: usize, input_vec: &mut Vec<Vec<(Option<usize>, char)>>, bmap: &mut BTreeMap<(usize, usize), bool>){
    let mut recur1 = false;
    let mut recur2 = false;
    let mut recur3 = false;
    let mut recur4 = false;
    
    depth += 1;
    
    bmap.insert(inspect, true);
    let mut tmp_coord: (usize, usize);
    if inspect.0 < dimx - 1{
        tmp_coord = (inspect.0+1, inspect.1);
        if input_vec[inspect.1][inspect.0].1 as u8 <= input_vec[tmp_coord.1][tmp_coord.0].1 as u8 + 1{
            if None == input_vec[tmp_coord.1][tmp_coord.0].0 || (None != input_vec[tmp_coord.1][tmp_coord.0].0 && Some(depth) < input_vec[tmp_coord.1][tmp_coord.0].0){
                recur1 = true;
                input_vec[tmp_coord.1][tmp_coord.0].0 = Some(depth);
            }
        }
    }
    if inspect.1 < dimy - 1{
        tmp_coord = (inspect.0, inspect.1+1);
        if input_vec[inspect.1][inspect.0].1 as u8 <= input_vec[tmp_coord.1][tmp_coord.0].1 as u8 + 1{
            if None == input_vec[tmp_coord.1][tmp_coord.0].0 || (None != input_vec[tmp_coord.1][tmp_coord.0].0 && Some(depth) < input_vec[tmp_coord.1][tmp_coord.0].0){
                recur2 = true;
                input_vec[tmp_coord.1][tmp_coord.0].0 = Some(depth);
            }
        }
    }
    if inspect.0 > 0{
        tmp_coord = (inspect.0-1, inspect.1);
        if input_vec[inspect.1][inspect.0].1 as u8 <= input_vec[tmp_coord.1][tmp_coord.0].1 as u8 + 1{
            if None == input_vec[tmp_coord.1][tmp_coord.0].0 || (None != input_vec[tmp_coord.1][tmp_coord.0].0 && Some(depth) < input_vec[tmp_coord.1][tmp_coord.0].0){
                recur3 = true;
                input_vec[tmp_coord.1][tmp_coord.0].0 = Some(depth);
            }
        }
    }
    if inspect.1 > 0{
        tmp_coord = (inspect.0, inspect.1-1);
        if input_vec[inspect.1][inspect.0].1 as u8 <= input_vec[tmp_coord.1][tmp_coord.0].1 as u8 + 1{
            if None == input_vec[tmp_coord.1][tmp_coord.0].0 || (None != input_vec[tmp_coord.1][tmp_coord.0].0 && Some(depth) < input_vec[tmp_coord.1][tmp_coord.0].0){
                recur4 = true;
                input_vec[tmp_coord.1][tmp_coord.0].0 = Some(depth);
            }
        }
    }
    
    if recur1{
        tmp_coord = (inspect.0+1, inspect.1);
        if !bmap.contains_key(&tmp_coord){
            recurse(dimx, dimy, tmp_coord, depth, input_vec, bmap);
            bmap.remove(&tmp_coord);
        }
    }
    if recur2{
        tmp_coord = (inspect.0, inspect.1+1);
        if !bmap.contains_key(&tmp_coord){
            recurse(dimx, dimy, tmp_coord, depth, input_vec, bmap);
            bmap.remove(&tmp_coord);
        }
    }
    if recur3{
        tmp_coord = (inspect.0-1, inspect.1);
        if !bmap.contains_key(&tmp_coord){
            recurse(dimx, dimy, tmp_coord, depth, input_vec, bmap);
            bmap.remove(&tmp_coord);
        }
    }
    if recur4{
        tmp_coord = (inspect.0, inspect.1-1);
        if !bmap.contains_key(&tmp_coord){
            recurse(dimx, dimy, tmp_coord, depth, input_vec, bmap);
            bmap.remove(&tmp_coord);
        }
    }
}

pub fn solve_helper (input: &str) {
    
    let mut bmap: BTreeMap<(usize, usize), bool> = BTreeMap::new();
    
    let mut input_vec: Vec<Vec<(Option<usize>, char)>> = Vec::new();
    let mut end: (usize, usize) = (0,0);
    let mut start: (usize, usize) = (0,0);
    
    for (ln, line) in input.lines().enumerate(){
        input_vec.push(Vec::new());
        for (cn, c) in line.chars().enumerate(){
            if c == 'E'{
                end = (cn, ln);
                input_vec[ln].push((Some(0), 'z'));
            }else if c == 'S'{
                start = (cn, ln);
                input_vec[ln].push((None, 'a'));
            }else{
                input_vec[ln].push((None, c));
            }
        }
    }
    let dimx = input_vec[0].len();
    let dimy = input_vec.len();
    
    recurse(dimx, dimy, end, 0, &mut input_vec, &mut bmap);
    
    println!("Part 1: {}", input_vec[start.1][start.0].0.unwrap());
    
    let mut ret: Option<usize> = None;
    for line in input_vec{
        for spot in line{
            /*
            if 'E' == spot.1{
                print!("   E");
            }else if None != spot.0{
                print!("{:>4}", spot.0.unwrap());
            }else{
                print!("   x");
            }
            */
            if 'a' == spot.1{
                if None == ret{
                    ret = spot.0;
                }else if ret > spot.0 && None != spot.0{
                    ret = spot.0;
                }
            }
        }
        //println!("\n\n\n");
    }
    println!("Part 2: {}", ret.unwrap());
    
}

pub fn solve(release: bool){
    let input: &str;
    if release{
        input = include_str!("../inputs/day_12_release.txt");
    }else{
        input = include_str!("../inputs/day_12_test.txt");
    }
    solve_helper(input);
}
