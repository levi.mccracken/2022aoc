use nom::{
    bytes::complete::tag,
    character::complete::{u8 as u8_parser},
    multi::separated_list0,
    sequence::{delimited},
    IResult,
};
use std::cmp;
enum Expr {
    Num(u8),
    List(Vec<Expr>),
}

fn expression_parser(i: &str) -> IResult<&str, Expr> {
    if i.starts_with("[") {
        let (rest, list) = list_parser(i)?;
        Ok((rest, Expr::List(list)))
    } else {
        let (rest, num) = u8_parser(i)?;
        Ok((rest, Expr::Num(num)))
    }
}

fn list_parser(i: &str) -> IResult<&str, Vec<Expr>> {
    let (rest, items) = delimited(
        tag("["),
        separated_list0(tag(","), expression_parser),
        tag("]"),
    )(i)?;
    Ok((rest, items))
}

fn compare(left: &Vec<Expr>, right: &Vec<Expr>) -> (bool, bool){
    let length = cmp::min(left.len(), right.len());
    for index in 0..length{
        if let Expr::Num(l) = left[index]{
            if let Expr::Num(r) = right[index]{
                if l > r{
                    return (true, false);
                }
                if r > l{
                    return (true, true);
                }
            }
            if let Expr::List(r) = &right[index]{
                let res = compare(&vec![Expr::Num(l)], r);
                if true == res.0{
                    return res;
                }
                if false == res.1{
                    return (false, false);
                }
            }
        }else if let Expr::List(l) = &left[index]{
            if let Expr::Num(r) = right[index]{
                let res = compare(l, &vec![Expr::Num(r)]);
                if true == res.0{
                    return res;
                }
                if false == res.1{
                    return (false, false);
                }
            }else if let Expr::List(r) = &right[index]{
                let res = compare(l, r);
                if true == res.0{
                    return res;
                }
                if false == res.1{
                    return (false, false);
                }
            }
        }
    }
    if right.len() < left.len(){
        return (true, false);
    }
    if right.len() > left.len(){
        return (true, true);
    }
    (false, true)
}

fn part_01(input: &str){
    let mut sum = 0;
    for (index, pair) in input.split("\n\n").enumerate(){
        let pair_lines: Vec<&str> = pair.lines().collect();
        let first = pair_lines[0];
        let second = pair_lines[1];

        let left = list_parser(first);
        let right = list_parser(second);

        if compare(&left.unwrap().1, &right.unwrap().1).1{
            //println!("{}", index + 1);
            sum += index + 1;
        }
    }
    println!("Part 1: {sum}");
}

pub fn solve(release: bool){
    let input: &str;
    if release{
        input = include_str!("../inputs/day_13_release.txt");
    }else{
        input = include_str!("../inputs/day_13_test.txt");
    }
    part_01(input);
}