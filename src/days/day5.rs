fn part_01(count: i32, two_dim: &mut Vec<Vec<char>>, from: i32, to: i32){
    for _i in 0..count{
        let crat = two_dim[from as usize - 1].pop().unwrap();
        two_dim[to as usize - 1].push(crat);
    }
}

fn part_02(count: i32, two_dim: &mut Vec<Vec<char>>, from: i32, to: i32){
    let mut local_stack = Vec::new();
    for _i in 0..count{
        let crat = two_dim[from as usize - 1].pop().unwrap();
        local_stack.push(crat);
    }
    for _i in 0..count{
        let crat = local_stack.pop().unwrap();
        two_dim[to as usize - 1].push(crat);
    }
}

pub fn solve_helper(input: &str, part: i32) {

    let mut input = input.lines();
    let mut structure_strings = Vec::new();
    
    loop{
        let line = input.next().unwrap();
        if line == ""{
            break;
        }
        structure_strings.push(line);
    }
    
    let mut two_dim: Vec<Vec<char>> = Vec::new();
    
    
    let mut columns = 0;
    for c in structure_strings.pop().unwrap().chars(){
        let casi32 = c as i32 - '0' as i32;
        if casi32 > 0 && casi32 <= 9{
            columns = casi32;
        }
    }
    //println!("{}", columns);
    for _i in 0..columns{
        two_dim.push(Vec::new());
    }
    
    loop{
        match structure_strings.pop(){
            None => break,
            Some(line) => {
                let mut row = line.chars();
                row.next();
                let c1 = row.next();
                if c1 != None{
                    let c1unwrap = c1.unwrap();
                    if ' ' != c1unwrap{
                        two_dim[0].push(c1unwrap);
                    }
                }
                for c in 1..columns{
                    row.next();
                    row.next();
                    row.next();
                    let c_other = row.next();
                    if c_other != None{
                        let c_other_unwrap = c_other.unwrap();
                        if ' ' != c_other_unwrap{
                            two_dim[c as usize].push(c_other_unwrap);
                        }
                    }
                }
            }
        };
    }
    
    //println!("{:?}", two_dim);
    
    loop{
        let line_opt = input.next();
        if line_opt == None{
            break;
        }
        let mut line = line_opt.unwrap().split(" ");
        line.next();
        let count = line.next().unwrap().parse::<i32>().unwrap();
        line.next();
        let from = line.next().unwrap().parse::<i32>().unwrap();
        line.next();
        let to = line.next().unwrap().parse::<i32>().unwrap();
        //println!("move {} from {} to {}", count, from, to);
        if part == 1{
            part_01(count, &mut two_dim, from, to);
        }else if part == 2{
            part_02(count, &mut two_dim, from, to);
        }
        //println!("{:?}", two_dim);
    } 
    
    //println!("");
    for c in 0..columns{
       print!("{}", two_dim[c as usize].pop().unwrap());
    }
    println!("");
}

pub fn solve(release: bool){
    let input: &str;
    if release{
        input = include_str!("../inputs/day_05_release.txt");
    }else{
        input = include_str!("../inputs/day_05_test.txt");
    }
    solve_helper(input, 1);
    solve_helper(input, 2);
}
