
struct Monkey{
    inventory: Vec<u128>,
    inspect: Box<dyn Fn(u128) -> u128>,
    true_monkey: usize,
    false_monkey: usize,
    div_by: u128,
}
impl Monkey{
    pub fn get_worry(&self, item: u128 , div: bool) -> u128 {
        let mut worry = (self.inspect)(item);
        if div{
            worry = worry/3;
        }
        worry
    }
    pub fn test(&self, worry: u128 ) -> bool{
        worry % self.div_by == 0
    }
}

fn solve_helper(input: &str, iters: usize, div: bool) -> u128{
    let mut monkies: Vec<Monkey> = Vec::new();
    for monkey_lines in input.split("Monkey "){
        if "" == monkey_lines{
            continue;
        }
        //let monkey_index = monkey_lines[0..1].parse::<usize>().unwrap();
        let mut monkey_inventory: Vec<u128 > = Vec::new();
        let inv_line_start = &monkey_lines[(monkey_lines.find("items: ").unwrap() + 7)..];
        let inv_line = &inv_line_start[..inv_line_start.find("\n").unwrap()];
        for item in inv_line.split(", "){
            monkey_inventory.push(item.parse::<u128>().unwrap());
        }
        
        let op_str_start = &monkey_lines[(monkey_lines.find("Operation: new = old ").unwrap() + 21)..];
        let op_str = &op_str_start[..op_str_start.find("\n").unwrap()];
        
        let monkey_inspect: Box<dyn Fn(u128) -> u128>; //Should never be used
        match op_str[0..=0].to_string().as_str(){
            "*" => {
                if op_str.contains("old"){
                    monkey_inspect = Box::new(|i: u128 | -> u128 {i * i});
                }else{
                    let moved = op_str[2..].parse::<u128>().unwrap();
                    monkey_inspect = Box::new(move |i: u128 | -> u128 {i * moved});
                }
            },
            "+" => {
                if op_str.contains("old"){
                    monkey_inspect = Box::new(|i: u128 | -> u128 {i + i});
                }else{
                    let moved = op_str[2..].parse::<u128>().unwrap();
                    monkey_inspect = Box::new(move |i: u128 | -> u128 {i + moved});
                }
            },
            _ => {
                println!("{}", op_str);
                panic!();
            },
        };
        
        let test_start = &monkey_lines[(monkey_lines.find("Test").unwrap()+"Test: divisible by ".len())..];
        let monkey_test = test_start[0..test_start.find("\n").unwrap()].parse::<u128>().unwrap();
        
        let true_start = &monkey_lines[(monkey_lines.find("true").unwrap())..];
        let false_start = &monkey_lines[(monkey_lines.find("false").unwrap())..];
        
        let true_newline = true_start.find("\n").unwrap();
        let false_newline_option = false_start.find("\n");
        
        let true_num = true_start[true_newline-1..true_newline].parse::<usize>().unwrap();
        let false_num: usize;
        if false_newline_option != None{
            let false_newline = false_newline_option.unwrap();
            false_num = false_start[false_newline-1..false_newline].parse::<usize>().unwrap();
        }else{
            false_num = false_start[false_start.len()-1..].parse::<usize>().unwrap();
        }
        
        monkies.push(Monkey{inventory: monkey_inventory, inspect: monkey_inspect, true_monkey: true_num, false_monkey: false_num, div_by: monkey_test});
    }
    
    let mut lcm = 1;
    for monkey in &monkies{
        lcm = num::integer::lcm(lcm, monkey.div_by);
        //println!("{}", monkey.div_by);
    }
    //println!("{}\n", lcm);
    let mut handle_item_counts: Vec<u128> = vec![0; monkies.len()];
    for _i in 0..iters{
        for monkey_index in 0..monkies.len(){
            for item_index in 0..monkies[monkey_index].inventory.len(){
                handle_item_counts[monkey_index] += 1;
                let worry = monkies[monkey_index].get_worry(monkies[monkey_index].inventory[item_index] % lcm, div);
                let dest_monkey_num: usize;
                if monkies[monkey_index].test(worry){
                    dest_monkey_num = monkies[monkey_index].true_monkey;
                }else{
                    dest_monkey_num = monkies[monkey_index].false_monkey;
                }
                monkies[dest_monkey_num].inventory.push(worry);
            }
            monkies[monkey_index].inventory = Vec::new();
        }
    }
    /*
    for monkey in &monkies{
        println!("{:?}", monkey.inventory);
    }
    */
    //println!("");
    handle_item_counts.sort();
    handle_item_counts.reverse();
    //println!("handles: {:?}", handle_item_counts);
    
    handle_item_counts[0] * handle_item_counts[1]
}
 
pub fn solve(release: bool){
    let input: &str;
    if release{
        input = include_str!("../inputs/day_11_release.txt");
    }else{
        input = include_str!("../inputs/day_11_test.txt");
    }
    println!("Part 1: {}", solve_helper(input, 20, true));
    println!("Part 2: {}", solve_helper(input, 10_000, false));
}
