
fn solve_01(input: &str) {
    let input_vec: Vec<char> = input.chars().collect();
    for i in 3..input.chars().count(){
        if input_vec[i] != input_vec[i-1] && input_vec[i] != input_vec[i-2] && input_vec[i] != input_vec[i-3]{
            if input_vec[i-1] != input_vec[i-2] && input_vec[i-1] != input_vec[i-3]{
                if input_vec[i-2] != input_vec[i-3]{
                    println!("{}-{}{}{}{}", i+1, input_vec[i-3], input_vec[i-2], input_vec[i-1], input_vec[i]);
                    break;
                }
            }
        }
    }
}

fn solve_02(input: &str) {
    let input_vec: Vec<char> = input.chars().collect();
    for i in 4..input.chars().count(){
    
        let mut chunk: Vec<char> = input_vec[i..i+14].to_vec();
        chunk.sort();
        chunk.dedup();
        let amount = chunk.len();
        if amount == 14{
            println!("{}", i+14);
            break;
        }
    }
}

pub fn solve(release: bool){
    let input: &str;
    if release{
        input = include_str!("../inputs/day_06_release.txt");
    }else{
        input = include_str!("../inputs/day_06_test.txt");
    }
    solve_01(input);
    solve_02(input);
}
