
fn solve_01(input: &str) {
let mut sum = 0;
    for line in input.lines(){
        let len = line.chars().count();
        let (first, second) = line.split_at(len/2);
        let mut breakvar = false;
        for c1 in first.chars(){
            for c2 in second.chars(){
                if c1 == c2{
                    breakvar = true;
                    if c1 >= 'a' && c1 <= 'z'{
                        sum += (c1 as i32 - 'a' as i32) + 1;
                    }
                    else if c1 >= 'A' && c1 <= 'Z'{
                        sum += (c1 as i32 - 'A' as i32) + 27;
                    }
                    break;
                }
            }
            if breakvar{
                break;
            }
        }
    }
    println!("{}", sum);
}

fn solve_02(input: &str) {
    let mut sum = 0;
    let mut elves = input.lines();
    loop{
        let elf1 = elves.next();
        if elf1 == None{
            println!("{}", sum);
            break;
        }
        let elf2 = elves.next();
        let elf3 = elves.next();
        let mut breakvar = false;
        for c1 in elf1.unwrap().chars(){
            for c2 in elf2.unwrap().chars(){
                for c3 in elf3.unwrap().chars(){
                    if c1 == c2 && c2 == c3{
                        breakvar = true;
                        if c1 >= 'a' && c1 <= 'z'{
                            sum += (c1 as i32 - 'a' as i32) + 1;
                        }
                        else if c1 >= 'A' && c1 <= 'Z'{
                            sum += (c1 as i32 - 'A' as i32) + 27;
                        }
                        break;
                    }
                }
                if breakvar{
                    break;
                }
            }
            if breakvar{
                break;
            }
        }
    }
}


pub fn solve(release: bool){
    let input: &str;
    if release{
        input = include_str!("../inputs/day_03_release.txt");
    }else{
        input = include_str!("../inputs/day_03_test.txt");
    }
    solve_01(input);
    solve_02(input);
}
