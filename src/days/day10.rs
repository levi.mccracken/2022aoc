pub fn solve_helper(input: &str){
    let mut x: i32 = 1;
    let mut history: Vec<i32> = Vec::new();
    history.push(x);
    history.push(x);
    for line in input.lines(){
        match line[0..4].to_string().as_str(){
            "noop" => {
                history.push(x);
                
            },
            "addx" => {
                history.push(x);
                x += line[5..].parse::<i32>().unwrap();
                history.push(x);
            },
            &_ => {
                panic!();
            },
        }
    }
    
    //println!("{:?}", history);
    
    let mut sum = 0;
    for i in (0..history.len()).filter(|i| i % 40 == 20){
        sum += i as i32 * history[i];
        
        //println!("{}  * {}", i, history[i]);
    }
    println!("Part 1: {}", sum);
    
    let mut grid: Vec<char> = vec!(' '; 240);
    for i in 0..240 as i32{
        if (i%40)+1 >= history[1 + i as usize] && (i%40) - 1 <= history[1+ i as usize] {
            grid[i as usize] = '#';
        }
    }
    
    println!("Part 2:");
    for row in 0..6{
        for c in 40*row..40*row+40{
            print!("{}", grid[c]);
        }
        println!("");
    }
}

pub fn solve(release: bool){
    let input: &str;
    if release{
        input = include_str!("../inputs/day_10_release.txt");
    }else{
        input = include_str!("../inputs/day_10_test.txt");
    }
    solve_helper(input);
}
