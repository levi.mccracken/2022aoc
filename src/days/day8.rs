
use std::cmp;

pub fn solve_helper(input: &str) {
    let mut grid: Vec<Vec<char>> = Vec::new();
    for (line_c, line) in input.lines().enumerate(){
        grid.push(Vec::new());
        for chr in line.chars(){
            grid[line_c].push(chr);
        }
    }
    
    let max_width = grid[0].len() - 1;
    let max_height = grid.len() - 1;
   
    let mut vis = 0;
    for (i, line) in grid.iter().enumerate(){
        if 0 == i || max_height == i{
            vis += grid[i].len();
            continue;
        }
        for (j, c) in line.iter().enumerate(){
            if 0 == j || max_width == j{
                vis += 1;
                continue;
            }
            
            //Left
            let mut visable = true;
            for x in 0..j{
                if grid[i][x] >= *c{
                    visable = false;
                    break;
                }
            }
            if visable{
                vis += 1;
                continue;
            }
            
            //Right
            visable = true;
            for x in j+1..=max_width{
                if grid[i][x] >= *c{
                    visable = false;
                    break;
                }
            }
            if visable{
                vis += 1;
                continue;
            }
            
            //Up
            visable = true;
            for y in 0..i{
                if grid[y][j] >= *c{
                    visable = false;
                    break;
                }
            }
            if visable{
                vis += 1;
                continue;
            }
            
            //Down
            visable = true;
            for y in i+1..=max_height{
                if grid[y][j] >= *c{
                    visable = false;
                    break;
                }
            }
            if visable{
                vis += 1;
                continue;
            }
        }
    }
    
    let mut score = 0;
    for (i, line) in grid.iter().enumerate(){
        for (j, c) in line.iter().enumerate(){
            //Left
            let mut local_score1 = 0;
            let mut local_score2 = 0;
            let mut local_score3 = 0;
            let mut local_score4 = 0;
            for x in (0..j).rev(){
                local_score1 += 1;
                if grid[i][x] >= *c{
                    break;
                }
            }
            
            //Right
            for x in j+1..=max_width{
                local_score2 += 1;
                if grid[i][x] >= *c{
                    break;
                }
            }
            
            //Up
            for y in (0..i).rev(){
                local_score3 += 1;
                if grid[y][j] >= *c{
                    break;
                }
            }
            
            //Down
            for y in i+1..=max_height{
                local_score4 += 1;
                if grid[y][j] >= *c{
                    break;
                }
            }
            score = cmp::max(score, local_score1 * local_score2 * local_score3 * local_score4);
        }
    }
    
    println!("Part 1: {}", vis);
    println!("Part 2: {}", score);
}

pub fn solve(release: bool){
    let input: &str;
    if release{
        input = include_str!("../inputs/day_08_release.txt");
    }else{
        input = include_str!("../inputs/day_08_test.txt");
    }
    solve_helper(input);
}
