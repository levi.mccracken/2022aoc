
fn solve_01(input: &str) {
    let lines = input.lines();
    let mut score = 0;
    for line in lines{
        let mut linechars = line.chars();
        let them = linechars.next().unwrap();
        linechars.next();
        let me = linechars.next().unwrap();
        if them == 'A'{
            match me{
                'X' => score += 3,
                'Y' => score += 6,
                'Z' => score += 0,
                _   => println!("Error")
            }
        }
        if them == 'B'{
            match me{
                'X' => score += 0,
                'Y' => score += 3,
                'Z' => score += 6,
                _   => println!("Error")
            }
        }
        if them == 'C'{
            match me{
                'X' => score += 6,
                'Y' => score += 0,
                'Z' => score += 3,
                _   => println!("Error")
            }
        }
        match me{
            'X' => score += 1,
            'Y' => score += 2,
            'Z' => score += 3,
            _   => println!("Error")
        }
    }
    
    println!("{}", score);
}

fn solve_02(input: &str) {
    let lines = input.lines();
    let mut score = 0;
    for line in lines{
        let mut linechars = line.chars();
        let them = linechars.next().unwrap();
        linechars.next();
        let me = linechars.next().unwrap();
        if them == 'A'{ //rock
            match me{
                'X' => score += 3, //lose sic
                'Y' => score += 1, //draw rock
                'Z' => score += 2, //win paper
                _   => println!("Error")
            }
        }
        if them == 'B'{ //paper
            match me{
                'X' => score += 1, //lose rock
                'Y' => score += 2, //draw paper
                'Z' => score += 3, //win sic
                _   => println!("Error")
            }
        }
        if them == 'C'{ //sic
            match me{
                'X' => score += 2, //lose paper
                'Y' => score += 3, //draw sic
                'Z' => score += 1, //win rock
                _   => println!("Error")
            }
        }
        match me{
            'X' => score += 0,
            'Y' => score += 3,
            'Z' => score += 6,
            _   => println!("Error")
        }
    }
    
    println!("{}", score);
}

pub fn solve(release: bool){
    let input: &str;
    if release{
        input = include_str!("../inputs/day_02_release.txt");
    }else{
        input = include_str!("../inputs/day_02_test.txt");
    }
    solve_01(input);
    solve_02(input);
}
